#
# Audit Account - target bucket for GuardDuty
#

# ----- KMS -----

resource "aws_kms_key" "guardduty" {
  description         = "KMS key for S3 GuardDuty Bucket"
  enable_key_rotation = true
  policy              = data.aws_iam_policy_document.kms_guardduty.json

  tags = {
    Name = "guardduty"
  }
}

resource "aws_kms_alias" "guardduty" {
  name          = "alias/guardduty"
  target_key_id = aws_kms_key.guardduty.id
}

# ----- S3 -----

resource "aws_s3_bucket" "guardduty" {
  bucket = local.guardduty_bucket_name
  acl    = "private"

  tags = {
    Name         = local.guardduty_bucket_name
    Usage        = "guardduty"
    Organization = data.aws_organizations_organization.main.id
  }
}

# attach policy to bucket

resource "aws_s3_bucket_policy" "guardduty" {
  bucket = aws_s3_bucket.guardduty.id
  policy = data.aws_iam_policy_document.s3_guardduty.json
}

# detectors are created using tf_aws_audit_guardduty modules
