locals {
  cloudtrail_bucket_name = "${data.aws_organizations_organization.main.id}-cloudtrail"
  config_bucket_name     = "${data.aws_organizations_organization.main.id}-config"
  flowlogs_bucket_name   = "${data.aws_organizations_organization.main.id}-flowlogs"
  guardduty_bucket_name  = "${data.aws_organizations_organization.main.id}-guardduty"
  tfstate_bucket_name    = "${data.aws_organizations_organization.main.id}-tfstate"

  prefixes               = data.aws_s3_bucket_objects.config.common_prefixes
}

