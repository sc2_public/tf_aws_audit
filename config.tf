# Audit Account - target bucket for Config


# ----- KMS -----


resource "aws_kms_key" "config" {
  description         = "KMS key for S3 Config Bucket"
  enable_key_rotation = true
#  policy              = data.aws_iam_policy_document.kms_config.json

  tags = {
    Name = "config"
  }
}

resource "aws_kms_alias" "config" {
  name          = "alias/config"
  target_key_id = aws_kms_key.config.id
}


# ----- S3 -----

resource "aws_s3_bucket" "config" {
  bucket = local.config_bucket_name
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_alias.config.target_key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  # Object locking disabled because it appears to prevent
  # master account access to the bucket
  #
  # object_lock_configuration {
  #   object_lock_enabled = "Enabled"
  #   rule {
  #     default_retention {
  #       mode = var.config_bucket_retention_mode
  #       days = var.config_bucket_retention_days
  #     }
  #   }
  # }


  tags = {
    Name         = local.config_bucket_name
    Usage        = "config"
    Organization = data.aws_organizations_organization.main.id
  }
}

# attach policy to bucket

resource "aws_s3_bucket_policy" "config" {
  bucket = aws_s3_bucket.config.id
  policy = data.aws_iam_policy_document.s3_config.json
}

# ----- CONFIG -----

resource "aws_config_configuration_aggregator" "main" {
  name = "main"

  account_aggregation_source {
    account_ids = [
      for p in local.prefixes: trimprefix(trimsuffix(p, "/"), "AWSLogs/")
    ]
    regions     = var.config_regions
  }
}
