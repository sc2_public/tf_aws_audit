# Audit Account - target bucket for Terraform state


# ----- KMS -----


resource "aws_kms_key" "tfstate" {
  description         = "KMS key for S3 Terraform state bucket"
  enable_key_rotation = true

  tags = {
    Name = "tfstate"
  }
}

resource "aws_kms_alias" "tfstate" {
  name          = "alias/tfstate"
  target_key_id = aws_kms_key.tfstate.id
}


# ----- S3 -----
resource "aws_s3_bucket" "tfstate" {
  bucket = local.tfstate_bucket_name
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_alias.tfstate.target_key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  versioning {
    enabled    = true
    mfa_delete = false
  }

  tags   = {
    Name         = local.tfstate_bucket_name
    Usage        = "tfstate"
    Organization = data.aws_organizations_organization.main.id
    backup       = "daily14"
  }
}



