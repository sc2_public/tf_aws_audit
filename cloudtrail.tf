# Audit Account - target bucket for CloudTrail


# ----- KMS -----

resource "aws_kms_key" "cloudtrail" {
  description         = "KMS key for S3 Cloudtrail Bucket"
  enable_key_rotation = true

  tags = {
    Name = "cloudtrail"
  }
}

resource "aws_kms_alias" "cloudtrail" {
  name          = "alias/cloudtrail"
  target_key_id = aws_kms_key.cloudtrail.id
}


# ----- S3 -----

resource "aws_s3_bucket" "cloudtrail" {
  bucket = local.cloudtrail_bucket_name
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_alias.cloudtrail.target_key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  object_lock_configuration {
    object_lock_enabled = "Enabled"
    rule {
      default_retention {
        mode = var.cloudtrail_bucket_retention_mode
        days = var.cloudtrail_bucket_retention_days
      }
    }
  }

  tags = {
    Name         = local.cloudtrail_bucket_name
    Usage        = "cloudtrail"
    Organization = data.aws_organizations_organization.main.id
  }
}

# attach bucket policy to bucket

resource "aws_s3_bucket_policy" "cloudtrail" {
  bucket = aws_s3_bucket.cloudtrail.id
  policy = data.aws_iam_policy_document.s3_cloudtrail.json
}
