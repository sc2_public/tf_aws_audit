output "guardduty_bucket_arn" {
  value = aws_s3_bucket.guardduty.arn
}

output "guardduty_kms_arn" {
  value = aws_kms_alias.guardduty.target_key_arn
}
