# Audit Account - identities

data "aws_organizations_organization" "main" {}

data "aws_caller_identity" "audit" {}

data "aws_caller_identity" "terraform" {
  provider = aws.master
}

data "aws_region" "current" {}

data "aws_iam_policy_document" "iso_audit" {
  statement {
    actions = [
      "config:*",
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:List*",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ListQueues",
      "s3:Get*",
      "s3:List*",
    ]
    resources = [
      "*"
    ]
  }

  statement {
    sid       = "AllowAssumeRoleInTcgSecurityAccount"
    effect    =  "Allow"
    actions   = [
      "sts:AssumeRole"
    ]
    resources = [
      var.tcg_security_role
    ]
  }

}

# Enable write access to the bucket for cloudtrail.amazonaws.com principal

data "aws_iam_policy_document" "s3_cloudtrail" {

  # allow the master account and the terraform IAM user to get bucket info

  statement {
    sid        = "MasterAccountCheck"
    effect     = "Allow"
    actions    = [
      "s3:GetBucket*",
      "s3:ListBucket*"
    ]
    resources  = [ aws_s3_bucket.cloudtrail.arn ]
    principals {
      type        = "AWS"
      identifiers = [
        data.aws_organizations_organization.main.master_account_id,
        data.aws_caller_identity.terraform.arn
      ]
    }
  }

  # allow cloudtrail principal to check bucket ACL

  statement {
    sid        = "AWSCloudTrailAclCheck"
    effect     = "Allow"
    actions    = [ "s3:GetBucketAcl" ]
    resources  = [ aws_s3_bucket.cloudtrail.arn ]
    principals {
      type        = "Service"
      identifiers = [ "cloudtrail.amazonaws.com" ]
    }
  }

  # allow cloudtrail principal to write to bucket

  statement {
    sid       = "AWSCloudTrailWrite"
    effect    = "Allow"
    actions   = [ "s3:PutObject" ]
    resources = [ "${aws_s3_bucket.cloudtrail.arn}/AWSLogs/*" ]
    principals {
      type        = "Service"
      identifiers = [ "cloudtrail.amazonaws.com" ]
    }
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = [ "bucket-owner-full-control" ]
    }
  }
}

# AWS Config Service needs access to the config KMS key

data "aws_iam_policy_document" "kms_config" {

  policy_id = "key-default-1"

  # include the default KMS policy statement

  statement {
    sid       = "Enable IAM User Permissions"
    actions   = [
      "kms:*"
    ]
    resources = [
      "*"
    ]
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.audit.account_id}:root"
      ]
    }
  }

  # allow config.amazonaws.com to use the KMS key

  statement {
    sid       = "Allow Config to use the key"
    actions   = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "config.amazonaws.com" ]
    }
    principals {
      type        = "AWS"
      identifiers = [
        data.aws_organizations_organization.main.master_account_id,
        data.aws_caller_identity.terraform.arn
      ]
    }
  }
}

# Enable write access to the config bucket for config.amazonaws.com principal

data "aws_iam_policy_document" "s3_config" {

  statement {
    sid        = "AWSConfigBucketCheck"
    effect     = "Allow"
    actions    = [
      "s3:GetBucket*",
      "s3:ListBucket*"
    ]
    resources  = [ aws_s3_bucket.config.arn ]
    principals {
      type        = "Service"
      identifiers = [ "config.amazonaws.com" ]
    }
  }

  # allow config principal to write to bucket

  statement {
    sid       = "AWSConfigBucketDelivery"
    effect    = "Allow"
    actions   = [ "s3:PutObject" ]
    resources = [
      "${aws_s3_bucket.config.arn}/*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "config.amazonaws.com" ]
    }
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = [ "bucket-owner-full-control" ]
    }
  }
}

# Get a list of all the /AWSLogs/* directories (i.e. account IDs) from
# config bucket

data "aws_s3_bucket_objects" "config" {
  bucket = local.config_bucket_name
  delimiter = "/"
  prefix = "AWSLogs/"
}

# delivery.logs.amazonaws.com needs access to the flowlogs KMS key

data "aws_iam_policy_document" "kms_flowlogs" {

  policy_id = "key-default-1"

  # include the default KMS policy statement

  statement {
    sid       = "Enable IAM User Permissions"
    actions   = [
      "kms:*"
    ]
    resources = [
      "*"
    ]
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.audit.account_id}:root"
      ]
    }
  }

  # allow delivery.logs.amazonaws.com to use the KMS key

  statement {
    sid       = "Allow VPC Flow Logs to use the key"
    actions   = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "delivery.logs.amazonaws.com" ]
    }
  }
}

# FlowLogs Bucket Policy

data "aws_iam_policy_document" "s3_flowlogs" {

  policy_id = "AWSLogDeliveryWrite20150319"

  # Allow writes by the deliver.logs.amazonaws.com principal

  statement {
    sid       = "AWSLogDeliveryWrite"
    effect    = "Allow"
    actions   = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.flowlogs.arn}/*"
    ]

    principals {
      type        = "Service"
      identifiers = [ "delivery.logs.amazonaws.com" ]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = [ "bucket-owner-full-control" ]
    }
  }

  # Allow ACL checking by delivery.logs.amazonaws.com principal

  statement {
    sid        = "AWSLogDeliveryAclCheck"
    effect     = "Allow"
    actions    = [
      "s3:GetBucketAcl"
    ]
    resources  = [
      aws_s3_bucket.flowlogs.arn
    ]

    principals {
      type        = "Service"
      identifiers = [
        "delivery.logs.amazonaws.com"
      ]
    }
  }
}

# guardduty.amazonaws.com needs access to the KMS key

data "aws_iam_policy_document" "kms_guardduty" {

  policy_id = "key-default-1"

  # include the default KMS policy statement

  statement {
    sid       = "Enable IAM User Permissions"
    actions   = [
      "kms:*"
    ]
    resources = [
      "*"
    ]
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.audit.account_id}:root"
      ]
    }
  }

  # allow guardduty.amazonaws.com to use the KMS key

  statement {
    sid       = "Allow GuardDuty to use the key"
    actions   = [
      "kms:GenerateDataKey*",
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
  }
}

# Enable write access to the bucket for guardduty.amazonaws.com principal

data "aws_iam_policy_document" "s3_guardduty" {

  # deny non-HTTPS access

  statement {
    sid = "Deny non-HTTPS access"
    effect = "Deny"
    actions = [
      "s3:*"
    ]
    resources = [
      "${aws_s3_bucket.guardduty.arn}/*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = [ "false" ]
    }
  }

  # require encryption with specific KMS key

  statement {
    sid = "Deny incorrect encryption header"
    effect = "Deny"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.guardduty.arn}/*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption-aws-kms-key-id"
      values   = [ aws_kms_alias.guardduty.target_key_arn  ]
    }
  }

  # require encrypted uploads

  statement {
    sid = "Deny unencrypted object uploads"
    effect = "Deny"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.guardduty.arn}/*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values   = [ "aws:kms" ]
    }
  }

  # allow guardduty principal to write to bucket

  statement {
    sid       = "Allow PutObject"
    actions   = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.guardduty.arn}/*"
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = [ "bucket-owner-full-control" ]
    }
  }

  # allow guardduty principal to get bucket location

  statement {
    sid       = "GuardDutyGetBucketLocation"
    effect    = "Allow"
    actions   = [
      "s3:GetBucketLocation"
    ]
    resources = [
      aws_s3_bucket.guardduty.arn
    ]
    principals {
      type        = "Service"
      identifiers = [ "guardduty.amazonaws.com" ]
    }
  }

}

