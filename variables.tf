variable "profile" {
  description = "Name of default AWS profile"
  type        = string
}

variable "cloudtrail_bucket_retention_mode" {
  type        = string
  description = "Cloudtrail S3 Bucket Object Lock Retention Mode"
  default     = "GOVERNANCE"
}

variable "cloudtrail_bucket_retention_days" {
  type        = string
  description = "Cloudtrail S3 Bucket Object Lock Retention Days"
  default     = "560"
}

variable "config_bucket_retention_mode" {
  type        = string
  description = "Config S3 Bucket Object Lock Retention Mode"
  default     = "GOVERNANCE"
}

variable "config_bucket_retention_days" {
  type        = string
  description = "Config S3 Bucket Object Lock Retention Days"
  default     = "560"
}

variable "flowlogs_bucket_retention_mode" {
  type        = string
  description = "FlowLogs S3 Bucket Object Lock Retention Mode"
  default     = "GOVERNANCE"
}

variable "flowlogs_bucket_retention_days" {
  type        = string
  description = "FlowLogs S3 Bucket Object Lock Retention Days"
  default     = "560"
}

variable "guardduty_bucket_retention_mode" {
  type        = string
  description = "GuardDuty S3 Bucket Object Lock Retention Mode"
  default     = "GOVERNANCE"
}

variable "guardduty_bucket_retention_days" {
  type        = string
  description = "GuardDuty S3 Bucket Object Lock Retention Days"
  default     = "560"
}

variable "config_regions" {
  type        = list
  description = "Config Regions"
}

variable "tcg_security_role" {
  type        = string
  description = "TCG Security Role ARN"
  default     = "arn:aws:iam::556902127208:role/Allow_iso_audit_account_to_pull_logs"
}
