# Audit Account - target bucket for VPC Flow Logs

# ----- KMS -----

resource "aws_kms_key" "flowlogs" {
  description         = "KMS key for S3 FlowLogs Bucket"
  enable_key_rotation = true
  policy              = data.aws_iam_policy_document.kms_flowlogs.json

  tags = {
    Name = "flowlogs"
  }
}

resource "aws_kms_alias" "flowlogs" {
  name          = "alias/flowlogs"
  target_key_id = aws_kms_key.flowlogs.id
}


# ----- S3 -----

resource "aws_s3_bucket" "flowlogs" {
  bucket   = local.flowlogs_bucket_name
  acl      = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_alias.flowlogs.target_key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  object_lock_configuration {
    object_lock_enabled = "Enabled"
    rule {
      default_retention {
        mode = var.flowlogs_bucket_retention_mode
        days = var.flowlogs_bucket_retention_days
      }
    }
  }

  tags = {
    Name         = local.flowlogs_bucket_name
    Usage        = "flowlogs"
    Organization = data.aws_organizations_organization.main.id
  }
}

# attach policy to bucket

resource "aws_s3_bucket_policy" "flowlogs" {
  bucket = aws_s3_bucket.flowlogs.id
  policy = data.aws_iam_policy_document.s3_flowlogs.json
}
