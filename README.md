# tf_aws_audit

Configures the organization audit account

* Creates KMS keys for buckets
* Creates S3 buckets for CloudTrail, Config, Flowlogs, and GuardDuty with appropriate policies
* Creates a Configuration Aggregator in the primary region


## NOTES

A PGP/GPG key is need to encrypted the ISO audit account's secret key. This key must be stored in a file called `pgp_key` in the root of the account's Terraform directory. To create the file, run

```
gpg -a --export KEYID > pgp_key
```

Next, edit the file and delete the first all the lines from the top of the file down to and including the blank line (the `-----BEGIN PGP PUBLIC KEY BLOCK-----` line, the comment line, and the empty line); them remove the last two lines (the very short base64 encoded line, and the `-----END PGP PUBLIC KEY BLOCK-----` line.

## Variables

| Name                               | Type       | Description                                     | Default        |
|------------------------------------|------------|-------------------------------------------------|----------------|
| `profile`                          | string     | Name of AWS profile to use for audit account    | _none_         |
| `cloudtrail_bucket_retention_mode` | string     | Cloudtrail S3 Bucket Object Lock Retention Mode | GOVERNANCE     |
| `cloudtrail_bucket_retention_days` | string     | Cloudtrail S3 Bucket Object Lock Retention Days | 560            |
| ~~`config_bucket_retention_mode`~~ | ~~string~~ | ~~Config S3 Bucket Object Lock Retention Mode~~ | ~~GOVERNANCE~~ |
| ~~`config_bucket_retention_days`~~ | ~~string~~ | ~~Config S3 Bucket Object Lock Retention Days~~ | ~~560~~        |
| `flowlogs_bucket_retention_mode`   | string     | FlowLogs S3 Bucket Object Lock Retention Mode   | GOVERNANCE     |
| `flowlogs_bucket_retention_days`   | string     | FlowLogs S3 Bucket Object Lock Retention Days   | 560            |
| `guardduty_bucket_retention_mode`  | string     | GuardDuty S3 Bucket Object Lock Retention Mode  | GOVERNANCE     |
| `guardduty_bucket_retention_days`  | string     | GuardDuty S3 Bucket Object Lock Retention Days  | 560            |
| `config_regions`                   | list       | Config Regions                                  | _none_         |

_Config and Object Locking appear to be incompatible._

## Outputs

| Name                   | Type   | Description              |
|------------------------|--------|--------------------------|
| `guardduty_bucket_arn` | string | ARN of GuardDuty bucket  |
| `guardduty_kms_arn`    | string | ARN of GuardDuty KMS key |

## Usage

This module should only be instantiated once in the audit account, and will run in the default region.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "org-audit"
}

module "audit" {
  source         = "git::https://code.stanford.edu/sc2_public/tf_aws_audit.git"
  profile        = "org-audit"
  config_regions = [ "us-east-1", "us-east-2", "us-west-1", "us-west-2" ]
  providers      = {
    aws.master    = aws.master
    aws.us_east_1 = aws.us_east_1
    aws.us_east_2 = aws.us_east_2
    aws.us_west_1 = aws.us_west_1
    aws.us_west_2 = aws.us_west_2
  }
}

module "audit_us_west_2" {
  source     = "git::https://code.stanford.edu/sc2_public/tf_aws_audit_guardduty.git"
  profile    = "org-audit"
  bucket_arn = module.audit.guardduty_bucket_arn
  kms_arn    = module.audit.guardduty_kms_arn
  providers  = {
    aws = aws.us_west_2
  }
}

```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default/)
* [AWS Audit GuardDuty Setup](https://code.stanford.edu/sc2_public/tf_aws_audit_guardduty/)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
* [Guardduty setup](https://code.stanford.edu/sc2_public/tf_aws_account_guardduty)
