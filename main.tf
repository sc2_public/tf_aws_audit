# Audit Account - providers

provider "aws" {
  alias = "master"
}

provider "aws" {
  alias = "us_east_1"
}

provider "aws" {
  alias = "us_east_2"
}

provider "aws" {
  alias = "us_west_1"
}

provider "aws" {
  alias = "us_west_2"
}
