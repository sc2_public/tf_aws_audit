resource "aws_iam_group" "iso_audit" {
  name = "iso_audit"
}

resource "aws_iam_policy" "iso_audit" {
  name   = "iso_audit"
  policy = data.aws_iam_policy_document.iso_audit.json
}

resource "aws_iam_group_policy_attachment" "iso_audit" {
  group      = aws_iam_group.iso_audit.id
  policy_arn = aws_iam_policy.iso_audit.arn
}

resource "aws_iam_user" "iso_audit" {
  name = "iso_audit"
}

resource "aws_iam_user_group_membership" "iso_audit" {
  user = aws_iam_user.iso_audit.name
  groups = [
   aws_iam_group.iso_audit.name
  ]
}

resource "aws_iam_access_key" "iso_audit" {
  user    = aws_iam_user.iso_audit.name
  pgp_key = file("${path.root}/pgp_key")
  lifecycle {
    ignore_changes = [
      secret,
      encrypted_secret,
      ses_smtp_password_v4
    ]
  }
}
